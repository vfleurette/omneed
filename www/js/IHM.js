
//___________________________________________________________________________________________________
function loadHeader(main_contentID , TypeCompte , numero , codeID ){
	"use strict";
	var Title;
	var BG;
  var header;
	if ( TypeCompte ==="Territory" ){ header="Territory", Title="Mes Sociétés"; BG="background:#2980B9;"; }
	else if ( TypeCompte === "RedList" ){header="RedList",Title="RedList"; BG="background:#C0392B;";}
	else if ( TypeCompte === "Competitors" ){ header="Competitors", Title="Concurrents"; BG="background:#8E44AD;";}
	else if ( TypeCompte === "AccountLead" ){header="AccountLead",  Title="Leads Sociétés"; BG="background:#27AE60;";}
	else if ( TypeCompte === "ContactLead" ){ header="ContactLead",Title="Leads Personne"; BG="background:#16A085;";}
	else if ( TypeCompte === "Options" ){header="Options",Title="Gestion des alertes"; BG="background:#2C3E50;";}
	else if ( TypeCompte === "Help" ){header="Help",Title="Aides"; BG="background:#2C3E50;";}
	var contenthtml='<div class="ui-icon-nodisc" data-role="header" data-theme="a"  style="'+BG+';text-shadow: none;" data-position="fixed"  data-id="header'+header+'"   data-url="header'+codeID+'">';
	    contenthtml=contenthtml+'<h1><font color=white >'+Title+'</font></h1>';
	    contenthtml=contenthtml+'<a href="#leftpanel'+numero+'" id="panel"  data-role="button" data-theme="ios" data-inline="true"><font size="5.5" style="text-shadow: none;border:none;" > &bull; &bull; &bull;</font></a>';

	    contenthtml=contenthtml+'</div><!-- /header -->';
	    document.getElementById(main_contentID).innerHTML=contenthtml;
	    var menuPanel = document.getElementById("panel");
	    new NoClickDelay(menuPanel);
}
//___________________________________________________________________________________________________
function loadHeaderOptions(main_contentID,numero){
	"use strict";
  var header="Options",Title="Gestion des alertes", BG="background:#2C3E50;"; 
	var contenthtml='<div class="ui-icon-nodisc" data-role="header" data-theme="a"  style="'+BG+';text-shadow: none;" data-position="fixed"  data-id="header'+header+'"   data-url="headerOption">';
	    contenthtml=contenthtml+'<h1>'+Title+'</h1>';

            contenthtml=contenthtml+'<a href="#popupOption" id="panelOption"  data-rel="popup" data-position-to="window" data-role="button" data-inline="true" data-transition="pop" data-role="button" data-theme="ios" style="text-shadow: none;"  data-shadow="false" data-inline="true"><font size="5.5" style="text-shadow: none;border:none;" >&bull; &bull; &bull;</font></a>';

                    contenthtml=contenthtml+'<div data-role="popup" id="popupOption" data-theme="c" data-shadow="false" data-dismissible="true"  data-overlay-theme="c" class="ui-content">';
                        contenthtml=contenthtml+'<h3>Enregistrer les changements</h3>';
                        contenthtml=contenthtml+'<p></p>';
                        contenthtml=contenthtml+'<div data-role="controlgroup" class="ui-icon-alt centrage"data-type="horizontal" data-mini="true" >';
                            contenthtml=contenthtml+'<a href="#" data-role="button" data-mini="true" data-shadow="false" data-inline="true" data-transition="slide" data-theme="c" onclick="ActionMemoriser();">Memoriser</a>';
                            contenthtml=contenthtml+'<a href="#leftpanel10" data-role="button" data-theme="c" data-shadow="false" data-inline="true"  data-transition="slide" data-mini="true">Annuler</a>';
                        contenthtml=contenthtml+'</div>';
	    contenthtml=contenthtml+'</div><!-- /header -->';
	    document.getElementById(main_contentID).innerHTML=contenthtml;
	    var menuPanelOption = document.getElementById("panelOption");
	    new NoClickDelay(menuPanelOption);
}
//___________________________________________________________________________________________________
function loadFooterN(main_contentID,TypeCompte,codeID){
"use strict";
var BG;
	if ( TypeCompte ==="Territory" ){BG="#2980B9";}
	else if ( TypeCompte === "RedList" ){BG="#C0392B";}
	else if ( TypeCompte === "Competitors" ){BG="#8E44AD";}
        var contenthtml='<div data-role="footer" class="ui-icon-alt" data-theme="c" data-corners="false" data-position="fixed" data-id="footer" data-url="Navigation.html#'+TypeCompte+'N" data-tap-toggle="false">';
        	    contenthtml=contenthtml+'<div data-role="navbar">';
	        		contenthtml=contenthtml+'<ul>';
	        			contenthtml=contenthtml+'<li><a href="#"  data-inline="true"><font size="3" color ="'+BG+'" >Alertes</font></a></li>';
	        			contenthtml=contenthtml+'<li><a href="#'+TypeCompte+'" data-inline="true" data-prefetch="true" data-transition="slide" >Sociétés</a></li>';
	        		contenthtml=contenthtml+'</ul>';
        				    contenthtml=contenthtml+'</div><!-- /navbar -->';
        	contenthtml=contenthtml+'</div><!-- /footer -->';
document.getElementById(main_contentID).innerHTML=contenthtml;
}
//___________________________________________________________________________________________________
function loadFooter(main_contentID,TypeCompte,codeID){
"use strict";
var BG;
	if ( TypeCompte ==="Territory" ){BG="#2980B9";}
	else if ( TypeCompte === "RedList" ){BG="#C0392B";}
	else if ( TypeCompte === "Competitors" ){BG="#8E44AD";}
	var contenthtml='<div data-role="footer" class="ui-icon-alt" data-theme="c" data-position="fixed" data-id="footer" data-url="Navigation.html#'+TypeCompte+'" data-tap-toggle="false">';
    		contenthtml=contenthtml+'<div data-role="navbar">';
    			    	contenthtml=contenthtml+'<ul>';
    		        	contenthtml=contenthtml+'<li><a href="#'+TypeCompte+'N" data-prefetch="true" data-inline="true"data-transition="slide" data-direction="reverse" >Alertes</a></li>';
    			            contenthtml=contenthtml+'<li><a href="#" data-inline="true" ><font size="3"  color ="'+BG+'" >Sociétés</font></a></li>';
    			        contenthtml=contenthtml+'</ul>';
    		contenthtml=contenthtml+'</div><!-- /navbar -->';
		contenthtml=contenthtml+'</div><!-- /footer -->';
document.getElementById(main_contentID).innerHTML=contenthtml;

}
//_____________________________________________________________________________________________________________________
function menuResponsive(main_menuID, numero, code, typeMenu){
    var username = window.localStorage.getItem("usernameSTORE");
    var versionApp = window.localStorage.getItem("versionApp");
    var mobileDevice = '<br />   Device Cordova: '  + device.cordova  + '<br />' + 
                                'Device Platform: ' + device.platform + '<br />' + 
                                'Device Version: '  + device.version  + '<br />' +
                                'Version App: '  + versionApp  + '<br />';
    var BGTE = "background:#2980B9; text-shadow: none;border: 0px !important;border-width: 0px !important;", BGRE = "background:#C0392B; text-shadow: none;border:0px ;border-width: 0px !important;", BGCO = "background:#8E44AD; text-shadow: none;border:none;border-width: 0px !important;", BGAR = "background:#E67E22; text-shadow: none;border:none;border-width: 0px !important;", BGAL = "background:#27AE60; text-shadow: none;border:none;border-width: 0px !important;", BGCL = "background:#16A085; text-shadow: none;border:none;border-width: 0px !important;", BGOP = "background:#2C3E50; text-shadow: none;border:none;border-width: 0px !important;", BGBL = "background:#101010;", moquette = "background:#101010;";
    var contenthtml='<div data-role="panel" data-animate="true" data-theme="c" id="leftpanel'+numero+'"  style="background:#ECF0F1;" data-position="left" data-display="reveal" data-swipe-close="true">';
        contenthtml=contenthtml+'<ul data-theme="c" data-mini="true" data-role="listview" id="slidepanel" data-icon="false" data-count-theme="c"> ';

                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><center><span style="font-family:arial;font-size:18px;font-weight:bold;color:#ff6600;">om</span><span style="font-family:arial;font-size:18px;font-weight:bold;color:black;">need</span></center></li>';

        if(typeMenu==="Territory"){
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#" data-rel="close"  data-transition="slide"><FONT style="font-weight:bold;" color="#2980B9">Mes Sociétés</font></a></li>';
        }else{
                contenthtml=contenthtml+'<li  style="background:#ECF0F1;"><a href="#"  data-prefetch="true" data-transition="slide" onclick="PanelrefreshPage(\'Territory\',\'#TerritoryN\');"><FONT style="font-weight:bold;" color="#2980B9" >Mes Sociétés</font></a></li>';
        }
        
        if(typeMenu==="RedList"){
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#" data-rel="close"  data-transition="slide"><FONT style="font-weight:bold;" color="#C0392B" >RedList</font></a></li>';
        }else{
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#"  data-prefetch="true"  data-transition="slide" onclick="PanelrefreshPage(\'RedList\',\'#RedListN\');"><FONT style="font-weight:bold;" color="#C0392B" >RedList</font></a></li>';
        }
        if(typeMenu==="AccountLead"){
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#demo-links" data-rel="close"  data-transition="slide"><FONT style="font-weight:bold;" color="#27AE60" >Leads Société</font></a></li>';
        }else{
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#"  data-prefetch="true" data-transition="slide" onclick="PanelrefreshPage(\'AccountLead\',\'#LeadsN\');"><FONT style="font-weight:bold;" color="#27AE60" >Leads Société</font></a></li>';
        }
        if(typeMenu==="ContactLead"){
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#demo-links" data-rel="close"  data-transition="slide"><FONT style="font-weight:bold;" color="#16A085" >Leads Personne</font></a></li>';
        }else{
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#"  data-prefetch="true" data-transition="slide" onclick="PanelrefreshPage(\'ContactLead\',\'#Leads\');"><FONT style="font-weight:bold;" color="#16A085" >Leads Personne</font></a></li>';
        }
        if(typeMenu==="Competitors"){
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#demo-links" data-rel="close" data-transition="slide"><FONT style="font-weight:bold;" color="#8E44AD" >Concurrents</font></a></li>';
        }else{
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#" data-prefetch="true" data-transition="slide" onclick="PanelrefreshPage(\'Competitors\',\'#CompetitorsN\');"><FONT style="font-weight:bold;" color="#8E44AD" >Concurrents</font></a></li>';
        }
        if(typeMenu==="Options"){
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#demo-links" data-rel="close"  data-transition="slide"><FONT style="font-weight:bold;"  >Gestion des Alertes</font></a></li>';
        }else{
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#Options"  data-prefetch="true" data-transition="slide" onclick="PanelrefreshPage(\'Options\',\'#Options\');"><FONT style="font-weight:bold;" >Gestion des Alertes</font></a></li>';
        }
        if(typeMenu==="Help"){
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#demo-links" data-rel="close" data-transition="slide"><b>Aide</b></a></li>';
        }else{
                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#Help"  data-prefetch="true" data-transition="slide"onclick="PanelrefreshPage(\'Help\',\'#Help\');"><b>Aide</b></a></li>';
        }

                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="mailto:vincent.fleurette@omneed.com?subject=Contact Omneed '+username+' &body='+username+'&nbsp;'+mobileDevice+'" data-transition="slide"><b>Contacter Omneed</b></a></li>';

                contenthtml=contenthtml+'<li style="background:#ECF0F1;"><a href="#Logout" id="Deconnexion" data-theme="a"  data-rel="popup" data-position-to="window" data-transition="turn"><b>Déconnexion</b></a></li>';
                    contenthtml=contenthtml+'<div data-role="popup" id="Logout" data-theme="c" data-shadow="false" data-dismissible="false" data-overlay-theme="c" class="ui-content">';
                        contenthtml=contenthtml+'<h3>Le souhaitez vous vraiment ?</h3>';
                        contenthtml=contenthtml+'<p>Cette action n\'affecte pas vos préférences </p>';
                        contenthtml=contenthtml+'<p>enregistrées sur le portail !</p>';
                        contenthtml=contenthtml+'<div data-role="controlgroup" class="ui-icon-alt centrage"data-type="horizontal" data-mini="true" >';
                            contenthtml=contenthtml+'<a href="#" data-role="button" data-mini="true" data-shadow="false" data-inline="true" data-transition="slide" data-theme="c" onclick="Deconnexion()">Déconnexion</a>';
                            contenthtml=contenthtml+'<a href="#" data-role="button" data-theme="c" data-shadow="false"data-rel="back" data-inline="true"  data-transition="slide" data-mini="true">Annuler</a>';
                        contenthtml=contenthtml+'</div>';
                    contenthtml=contenthtml+'</div>';
            contenthtml=contenthtml+'</ul>';
    contenthtml=contenthtml+'</div><!-- /leftpanel2 -->';
    document.getElementById(main_menuID).innerHTML=contenthtml;
    var slidePanel = document.getElementById("slidepanel");
    new NoClickDelay(slidePanel);
}