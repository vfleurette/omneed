/*(function ($) { $.fn.checkboxHierarchy = function (undefined) { */
//$(document).bind("mobileinit", function(){

(function ($) { $.fn.checkboxHierarchy = function () {
            $.extend($.expr[':'], {
                unchecked: function (obj) {
                    return ((obj.type == 'checkbox' || obj.type == 'radio') && !$(obj).is(':checked'));
                }
            });
			
            $("#tree input:checkbox").on('change', function () {
                $(this).parent('div').next('ul').find('input:checkbox').prop('checked', $(this).prop("checked"));

                for (var i = $('#tree').find('ul').length - 1; i >= 0; i--) {
                    $('#tree').find('ul:eq(' + i + ')').prev('div').children('input:checkbox').prop('checked', function () {
                        return $(this).parent('div').next('ul').find('input:unchecked').length === 0 ? true : false;
                    });
                }   
            });
};} (jQuery));

 (function ($) {
    $.SelectChildren = function (element) {
        if ($(element).is(':checked')) {
            $(element).parent().find(':checkbox').attr('checked', true);
        }
        else {
            $(element).parent().find(':checkbox').attr('checked', false);
        }
    }
})(jQuery);
