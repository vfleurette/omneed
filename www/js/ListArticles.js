//___________________________Territory____________________________________________________________________________________________
function loadMainContent(main_contentID, typeTE) {
'use strict';
    var username = window.localStorage.getItem("usernameSTORE");
    var token = window.localStorage.getItem("tokenSTORE");
    var responseValues = getAccountsList(username,token,typeTE);
        if(responseValues.length>0){
            if(responseValues.length>30){
                var contenthtml='<ul data-inset="false" data-role="listview" id="'+main_contentID+'list" data-autodividers="true" data-divider-theme="c" data-theme="c" data-filter-placeholder="Rechercher une société..." data-filter="true" data-icon="arrow-r" style="margin-left:1%!important; margin-right:1%!important;">';
                
            }else{
                var contenthtml='<ul data-inset="true" data-role="listview" id="'+main_contentID+'list" data-theme="c" classe="listecentrage"data-icon="arrow-r" >';
            }
                var m;
                for (m=0;m<responseValues.length;m++)
                {
                    contenthtml=contenthtml+'<li><a href="#" data-prefetch="true" data-transition="slide" onclick="LinktolisteNews(\'' +responseValues[m]+ '\',\''+typeTE+'\')"><font color="#848484">'+responseValues[m]+'</font></a></li>' ;
                }
            if(responseValues.length>=11){
                contenthtml=contenthtml+'<li data-role="list-divider" data-divider-theme="c" data-theme="c" data-count-theme="c">Nombre total de sociétés <span class="ui-li-count">'+responseValues.length+'</span></li>';
            }
                contenthtml=contenthtml+'</ul>';
                document.getElementById(main_contentID).innerHTML=contenthtml;
            }else{
                var contenthtml='<p><center><H1>Vous n\'avez pas ajouté de sociétés à votre territoire!</H1></center></p>';
                document.getElementById(main_contentID).innerHTML=contenthtml;
            }
}
//___________________________NEWS_ACCOUNT__________________________________________________________________________________________
function loadNewsAcount1(main_contentID){
"use strict";
    var value0 = window.localStorage.getItem("usernameSTORE");
    var value1 = window.localStorage.getItem("tokenSTORE");
    var value4 = window.localStorage.getItem("compte");
    var title = window.localStorage.getItem("compte");
    var typeLs = window.localStorage.getItem("typeLs");
        if ( typeLs ==="Territory" ){ var BG="background:#2980B9; text-shadow: none;border:none;"; }
        else if ( typeLs === "RedList" ){ var BG="background:#C0392B; text-shadow: none;border:none;";}
        else if ( typeLs === "Competitors" ){ var  BG="background:#8E44AD; text-shadow: none;border:none;";}
            var pageAccountNews= window.localStorage.getItem("pageAccount");
            var responseNews=getNews(value0,value1,value4,pageAccountNews);
        if(responseNews[1].length>0){
            var contenthtml='<div data-role="header" data-theme="a" style="'+BG+'" data-position="fixed">';
                    contenthtml=contenthtml+'<H1>'+title+'</H1>';
                    contenthtml=contenthtml+'<a href="./Navigation.html#'+typeLs+'"  data-theme="c"  data-ajax="false"  data-prefetch="true" class="ui-icon-nodisc" data-role="button" data-iconshadow="false" data-icon="arrow-l" data-iconpos="notext" data-direction="reverse" data-transition="slide">retour</a>';
                contenthtml=contenthtml+'</div>';
                contenthtml=contenthtml+'<div data-iscroll="" data-role="content">';
                    contenthtml=contenthtml+'<ul data-role="listview" id="'+main_contentID+'" data-icon="arrow-r" style="margin-left:1%!important; margin-right:1%!important;" data-inset="false">';
                        if(pageAccountNews>=2){
                            contenthtml=contenthtml+'<li  data-icon="false" data-mini="true"><a href="#"  id="prevTE" data-theme="c" data-count-theme="c" onclick="changementpageNewsMoins(\'' +pageAccountNews+ '\',\''+value4+'\')"><center>page precedente &#9650;</center></a></li>';
                        }                var j;
                for (j=0;j<responseNews[1].length;j++){
                    var contenthtmlconcept2=ColorConcept(responseNews[1][j][0]);
                     moment.lang('fr');
                    contenthtml=contenthtml+'<li><a href="#popupBasic'+j+'" data-rel="popup" data-transition="slidedown" ><H3>'+contenthtmlconcept2+'</H3><p class="ui-li-aside"><strong>'+moment(responseNews[1][j][4]).fromNow()+'</strong></p><p style=" color:#848484; height:50px; margin-bottom:0px; white-space: normal;">'+responseNews[1][j][1]+'</p></a></li>';
                        contenthtml=contenthtml+'<div data-role="popup" id="popupBasic'+j+'" class="ui-content" data-position-to="window" data-overlay-theme="c">';
                            contenthtml=contenthtml+'<a href="#" data-rel="back" data-role="button" data-theme="c" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>';
                            contenthtml=contenthtml+'<h3 class="ui-title"><center>'+title+'</center></h3>';
                            contenthtml=contenthtml+'<H3>'+contenthtmlconcept2+'</H3>';
                            contenthtml=contenthtml+'<p><strong>'+moment(responseNews[1][j][4]).fromNow()+'</strong></p>';
                            contenthtml=contenthtml+'<p>Source  <a href="" onclick="browser(\'' +responseNews[1][j][3]+ '\');" rel="external" data-theme="c">'+responseNews[1][j][2]+'</a></p>';
                            contenthtml=contenthtml+'<p style="text-overflow: ellipsis ; white-space: normal;">'+responseNews[1][j][1]+'</p>';
                        contenthtml=contenthtml+'<div data-role="controlgroup" class="ui-icon-alt centrage" data-type="horizontal" data-mini="true" >';
                                    contenthtml=contenthtml+'<a href="mailto:?subject=News Omneed &#32;'+title+'&#32;'+responseNews[1][j][0]+'&body=<b>'+title+'&#32;'+responseNews[1][j][0]+'</b> %0D%0A <i> source &#32;<a href='+responseNews[1][j][3]+'><u>'+responseNews[1][j][2]+'</u></a></i>%0D%0A'+moment(responseNews[1][j][4]).format('ll')+'%0D%0A'+responseNews[1][j][1]+'" data-role="button"  data-icon="edit" data-theme="c">Mail</a>';
                        contenthtml=contenthtml+'</div>';
                contenthtml=contenthtml+'</div>';
                }
                if(pageAccountNews < totaleadsAccount){
                    contenthtml=contenthtml+'<li data-icon="false" data-mini="true"><a href=""  id="nextTE" data-theme="c" data-count-theme="c" onclick="changementpageNewsPlus(\'' +pageAccountNews+ '\',\''+value4+'\')"><center>page suivante &#9660;</center></a></li>';
                }else{
                    contenthtml=contenthtml+'<hr/>';
                    contenthtml=contenthtml+'<li data-role="list-divider" data-divider-theme="c" data-theme="c" ><center><strong>Plus d\'autres news pour le moment</strong></center></li>';
                }
            contenthtml=contenthtml+'</ul>';
        contenthtml=contenthtml+'</div>';

        document.getElementById(main_contentID).innerHTML=contenthtml;
        var loadNewsAcount1 = document.getElementById(main_contentID);
        new NoClickDelay(loadNewsAcount1);
        }else{
          var contenthtml='<div data-role="header" data-theme="a" style="'+BG+'" data-position="fixed">';
                contenthtml=contenthtml+'<H1>'+title+'</H1>';
                contenthtml=contenthtml+'<a href="./Navigation.html#'+typeLs+'"  data-theme="c"  data-ajax="false"  data-prefetch="true" class="ui-icon-nodisc" data-role="button" data-iconshadow="false" data-icon="arrow-l" data-iconpos="notext" data-direction="reverse" data-transition="slide">retour</a>';
              contenthtml=contenthtml+'</div>';
              contenthtml=contenthtml+'<div data-iscroll="" data-role="content">';
                contenthtml=contenthtml+'<p><center><font color="#848484"><H1>Nous n\'avons aucune news a vous proposez pour le moment !</H1></center></p>';
              contenthtml=contenthtml+'</div>';
              document.getElementById(main_contentID).innerHTML=contenthtml;
}
}
//___________________________GetNotifications TE___________________________________________________________________________________
function notificationList(main_contentID,compteN){
"use strict";
var username = window.localStorage.getItem("usernameSTORE");
var token = window.localStorage.getItem("tokenSTORE");
var pageNotifTE= window.localStorage.getItem("page"+compteN);
var objdateAlert = window.localStorage.getItem("objdateAlert");
var responseNotif=getNotifications(username,objdateAlert,pageNotifTE,token,compteN);
var contenthtml='<ul data-icon="arrow-r" id="'+main_contentID+'" data-role="listview" data-inset="false"   style="margin-left:1%!important; margin-right:1%!important;"  data-theme="c">';
var f;
if(pageNotifTE>=2){
contenthtml=contenthtml+'<li  data-icon="false" data-mini="true"><a href="#"  id="prevRE" data-theme="c" onclick="changementpageMoins(\'' +pageNotifTE+ '\',\''+compteN+'\')"><center>page precedente &#9650;</center></a></li>';
}
if(responseNotif[1].length>=1){
for (f=0;f<responseNotif[1].length;f++)
{
var contenthtmlconcept= ColorConcept(responseNotif[1][f][1]);
 moment.lang('fr');
    contenthtml=contenthtml+'<li ><a  href="#popupBasic'+responseNotif[1][f][5]+'" data-rel="popup" data-transition="slidedown" data-position-to="window" ><H5>'+responseNotif[1][f][0]+'</H5><p><font size="2">'+contenthtmlconcept+'</font><p class="ui-li-aside"><strong>'+moment(responseNotif[1][f][6]).fromNow()+'</strong></p><p style=" color:#848484; height:50px; margin-bottom:0px; white-space: normal;">'+responseNotif[1][f][2]+'</p></a></li>'
    
            contenthtml=contenthtml+'<div data-role="popup" id="popupBasic'+responseNotif[1][f][5]+'" class="ui-content" data-position-to="window" data-overlay-theme="c">';
            contenthtml=contenthtml+'<a href="#" data-rel="back" data-role="button" data-theme="c" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>';
            contenthtml=contenthtml+'<h3 class="ui-title"><center>'+responseNotif[1][f][0]+'</center></h3>';
            contenthtml=contenthtml+'<H3>'+contenthtmlconcept+'</H3>';
            contenthtml=contenthtml+'<p>Source  <a href="" onclick="browser(\'' +responseNotif[1][f][4]+ '\');" rel="external" data-theme="c">'+responseNotif[1][f][3]+'</a></p>';
            contenthtml=contenthtml+'<p><i>'+moment(responseNotif[1][f][6]).fromNow()+'</i></p>';
            contenthtml=contenthtml+'<p style="white-space: normal;">'+responseNotif[1][f][2]+'</p>';
            contenthtml=contenthtml+'<div data-role="controlgroup" class="ui-icon-alt centrage"data-type="horizontal" data-mini="true">';
            contenthtml=contenthtml+'<a href="mailto:?subject=News Omneed &#32;'+responseNotif[1][f][0]+'&#32;'+responseNotif[1][f][1]+'&body=<b>'+responseNotif[1][f][0]+'&#32;'+responseNotif[1][f][1]+'</b> %0D%0A <i> source &#32;<a href='+responseNotif[1][f][4]+'><u>'+responseNotif[1][f][3]+'</u></a></i>%0D%0A'+moment(responseNotif[1][f][6]).format('ll')+'%0D%0A'+responseNotif[1][f][2]+'" data-role="button"  data-icon="edit" data-theme="c">Mail</a>';
contenthtml=contenthtml+'</div>';
contenthtml=contenthtml+'</div>';
}
}else{
        contenthtml=contenthtml+'<li data-icon="false" data-mini="true"><a href=""  id="nextRE" data-theme="c" ><center> il n\'y a pas d\'alerte antérieur disponible</center></a></li>';
}
if(responseNotif[1].length>=10){
        contenthtml=contenthtml+'<li data-icon="false" data-mini="true"><a href=""  id="nextRE" data-theme="c"  onclick="changementpagePlus(\'' +pageNotifTE+ '\',\''+compteN+'\')"><center>page suivante &#9660;</center></a></li>';
        }
contenthtml=contenthtml+'</ul>';
document.getElementById(main_contentID).innerHTML=contenthtml;
var theTapList = document.getElementById(main_contentID);
new NoClickDelay(theTapList);
}

//___________________________Lead Accounts________________________________________________________________________
function GetListALead(main_contentID,compteN){
"use strict";
var username = window.localStorage.getItem("usernameSTORE");
var token = window.localStorage.getItem("tokenSTORE");
var pageAL= window.localStorage.getItem("page"+compteN);
var dateAlertFR = window.localStorage.getItem("dateAlertFR");
var objdateAlert = window.localStorage.getItem("objdateAlert");
var responseAccountLead=getLeads(username,objdateAlert,pageAL,token,compteN);
navigator.splashscreen.hide();
//var totaleadsAL = Math.ceil(responseAccountLead[0]/10);
window.localStorage.setItem('nombreNotification'+compteN,responseAccountLead[0]);
var contenthtml='<ul data-icon="arrow-r" id="'+main_contentID+'" data-role="listview" data-inset="false" style="margin-left:1%!important; margin-right:1%!important;" data-theme="c">';
var alID;
if(pageAL>=2){
contenthtml=contenthtml+'<li   data-icon="false" data-mini="true"><a href="#"  id="prevAL" data-theme="c" onclick="changementpageMoins(\'' +pageAL+ '\',\''+compteN+'\')"><center>page precedente &#9650;</center></a></li>';
}
if(responseAccountLead[1].length>=1){
for (alID=0;alID<responseAccountLead[1].length;alID++)
{
      if(typeof responseAccountLead[1][alID][13]!=="undefined"){
    var contenthtmlconcept= ColorConcept(responseAccountLead[1][alID][13]);
    }else{
    var contenthtmlconcept= "";
    }
     moment.lang('fr');
    contenthtml=contenthtml+'<li ><a  href="#popupBasic'+responseAccountLead[1][alID][14]+'" data-rel="popup" data-transition="slidedown" data-position-to="window"><H5>'+contenthtmlconcept+'</H5><p style="text-overflow: ellipsis ; white-space: normal;"><strong>'+gestionUndefined(responseAccountLead[1][alID][5])+'</strong></p><p><font color="orange">C.A&nbsp;'+abbrNum(responseAccountLead[1][alID][2],"0")+'&nbsp;€</font>&nbsp;&nbsp;<font color="green"> Effectif '+abbrNum(responseAccountLead[1][alID][3],"2")+'&nbsp;p</font></p><p style=" color:#848484; height:35px; margin-bottom:0px; white-space: normal;">'+responseAccountLead[1][alID][12]+'</p><p><strong>'+ColorConcept(gestionUndefined(responseAccountLead[1][alID][0]))+'</strong>&nbsp;<font color"#848484">'+gestionUndefined(responseAccountLead[1][alID][8])+'&nbsp;&nbsp;'+gestionUndefined(responseAccountLead[1][alID][7])+'</font></p></a></li>';
        contenthtml=contenthtml+'<div data-role="popup" id="popupBasic'+responseAccountLead[1][alID][14]+'" class="ui-content" data-position-to="window" data-overlay-theme="c">';
        contenthtml=contenthtml+'<a href="#" data-rel="back" data-role="button" data-theme="c" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>';
        contenthtml=contenthtml+'<h3 class="ui-title"><center>'+responseAccountLead[1][alID][0]+'</center></h3>';
                contenthtml=contenthtml+'<H3>'+contenthtmlconcept+'</H3>';
                contenthtml=contenthtml+'<p>'+moment(responseAccountLead[1][alID][15]).fromNow()+'</p>';
                contenthtml=contenthtml+'<div data-role="collapsible-set" data-theme="c" data-content-theme="c" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"  data-iconshadow="false" data-shadow="false" >';
                contenthtml=contenthtml+'<div data-role="collapsible"  data-collapsed="false">';
                        contenthtml=contenthtml+'<h3>News</h3>';
                        contenthtml=contenthtml+'<p>Source  <a href="" onclick="browser(\'' +responseAccountLead[1][alID][11]+ '\');" rel="external" data-theme="c">'+responseAccountLead[1][alID][10]+'</a></p>';
                        contenthtml=contenthtml+'<p style="text-overflow: ellipsis ; white-space: normal;">'+responseAccountLead[1][alID][12]+'</p>';
                contenthtml=contenthtml+'</div>';
                
                    contenthtml=contenthtml+'<div data-role="collapsible">';
                        contenthtml=contenthtml+'<h3>Information</h3>';
                        contenthtml=contenthtml+'<p>'+responseAccountLead[1][alID][0]+'</p>';
                        contenthtml=contenthtml+'<p>Raison Sociale  '+responseAccountLead[1][alID][1]+'</p>';
                        contenthtml=contenthtml+'<p><font color="orange">C.A.  '+abbrNum(responseAccountLead[1][alID][2],"0")+'€</font></p>';
                        contenthtml=contenthtml+'<p><font color="green">effectif  '+abbrNum(responseAccountLead[1][alID][3],"2")+'p</font></p>';
                        contenthtml=contenthtml+'<p style="text-overflow: ellipsis ; white-space: normal;">Lib APE '+responseAccountLead[1][alID][5]+'</p>';
                        contenthtml=contenthtml+'<p>code APE  '+responseAccountLead[1][alID][4]+'</p>';
                    contenthtml=contenthtml+'</div>';
                    contenthtml=contenthtml+'<div data-role="collapsible">';
                        contenthtml=contenthtml+'<h3>Contact</h3>';
                        if(typeof responseAccountLead[1][alID][6]!=="undefined"){
                        contenthtml=contenthtml+'<p><a href="http://maps.apple.com/?q='+responseAccountLead[1][alID][6]+'&nbsp;'+responseAccountLead[1][alID][7]+'&nbsp;'+responseAccountLead[1][alID][8]+'">'+responseAccountLead[1][alID][6]+'</a></p>';
                        contenthtml=contenthtml+'<p>'+responseAccountLead[1][alID][7]+'&nbsp;'+responseAccountLead[1][alID][8]+'</p>';
                        }else{
                        contenthtml=contenthtml+'<p><a href="http://maps.apple.com/?q='+responseAccountLead[1][alID][7]+'&nbsp;'+responseAccountLead[1][alID][8]+'">'+responseAccountLead[1][alID][7]+'&nbsp;'+responseAccountLead[1][alID][8]+'</a></p>';
                        }
                        if(typeof responseAccountLead[1][alID][9]!=="undefined"){
                        contenthtml=contenthtml+'<p> <a href=tel:'+responseAccountLead[1][alID][9]+'>Tel:'+responseAccountLead[1][alID][9]+'</a></p>';
                        }
                    contenthtml=contenthtml+'</div>';
        contenthtml=contenthtml+'</div>';
contenthtml=contenthtml+'</div>';
}}else{
        contenthtml=contenthtml+'<li data-icon="false" data-mini="true"><a href=""  id="nextRE" data-theme="c" ><center> il n\'y a pas d\'alerte antérieur disponible</center></a></li>';
}
if(responseAccountLead[1].length>=10){
contenthtml=contenthtml+'<li  data-icon="false" data-mini="true"><a href=""  id="nextAL" data-theme="c"  onclick="changementpagePlus(\'' +pageAL+ '\',\''+compteN+'\')"><center>page suivante &#9660;</center></a></li>';
}
contenthtml=contenthtml+'</ul>';
document.getElementById(main_contentID).innerHTML=contenthtml;
var theTapList = document.getElementById(main_contentID);
new NoClickDelay(theTapList);

}
//___________________________Lead Accounts________________________________________________________________________
function GetListCLead(main_contentID,compteN){
"use strict";
var username = window.localStorage.getItem("usernameSTORE");
var token = window.localStorage.getItem("tokenSTORE");
var pageCL= window.localStorage.getItem("pageContactLead");
var dateAlertFR = window.localStorage.getItem("dateAlertFR");
var objdateAlert = window.localStorage.getItem("objdateAlert");
var responseContactLead=getLeads(username,objdateAlert,pageCL,token,compteN);
var contenthtml='';
contenthtml=contenthtml+'<ul data-icon="arrow-r" id="'+main_contentID+'" data-role="listview" data-inset="false" style="margin-left:1%!important; margin-right:1%!important;" data-theme="c">';
if(pageCL>=2){
contenthtml=contenthtml+'<li   data-icon="false" data-mini="true"><a href="#"  id="prevCL" data-theme="c"  onclick="changementpageMoins(\'' +pageCL+ '\',\''+compteN+'\')"><center>page precedente &#9650;</center></a></li>';
}
var clID;
if(responseContactLead[1].length>0){
for (clID=0;clID<responseContactLead[1].length;clID++)
{
    if(typeof responseContactLead[1][clID][13]!=="undefined"){
    var firstName=responseContactLead[1][clID][13];
    }else{
    var firstName="Mr/Mme";
    }
     moment.lang('fr');
    var contenthtmlconcept= ColorConcept(responseContactLead[1][clID][0]);
    contenthtml=contenthtml+'<li ><a href="#popupBasic'+responseContactLead[1][clID][17]+'" data-rel="popup" data-transition="slidedown" data-position-to="window"><H5>'+firstName+'&nbsp;'+responseContactLead[1][clID][14]+'</H5><p style="text-overflow: ellipsis ; white-space: normal;"><strong>'+responseContactLead[1][clID][15]+'</strong>&nbsp;'+contenthtmlconcept+'</p><p><font color="orange">C.A&nbsp;'+abbrNum(responseContactLead[1][clID][2],"0")+'&nbsp;€</font>&nbsp;&nbsp;<font color="green"> Effectif '+abbrNum(responseContactLead[1][clID][3],"2")+'&nbsp;p</font></p><p style=" color:#848484; height:35px; margin-bottom:0px; white-space: normal;">'+responseContactLead[1][clID][12]+'</p><p>'+responseContactLead[1][clID][7]+' '+responseContactLead[1][clID][8]+'</p></a></li>';
        contenthtml=contenthtml+'<div data-role="popup" id="popupBasic'+responseContactLead[1][clID][17]+'" class="ui-content" data-position-to="window" data-overlay-theme="c">';
                contenthtml=contenthtml+'<a href="#" data-rel="back" data-role="button" data-theme="c" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>';
                contenthtml=contenthtml+'<h4 class="ui-title" style="text-overflow: ellipsis ; white-space: normal;"><center>'+ColorConcept(firstName)+'&nbsp;'+ColorConcept(responseContactLead[1][clID][14])+'</center></h4>';
                contenthtml=contenthtml+'<h4 class="ui-title" style="text-overflow: ellipsis ; white-space: normal;"><center>'+responseContactLead[1][clID][15]+'</center></h4>';
                contenthtml=contenthtml+'<h4 class="ui-title" style="text-overflow: ellipsis ; white-space: normal;"><center>'+contenthtmlconcept+'</center></h4>';
                contenthtml=contenthtml+'<p><i>'+moment(responseContactLead[1][clID][18]).fromNow()+'</i></p>';
                                contenthtml=contenthtml+'<div data-role="collapsible-set" data-theme="c" data-content-theme="c" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d"  data-iconshadow="false" data-shadow="false" >';
                contenthtml=contenthtml+'<div data-role="collapsible"  data-collapsed="false">';
                    contenthtml=contenthtml+'<h3>News</h3>';
                    contenthtml=contenthtml+'<p>Source  <a href="" onclick="browser(\'' +responseContactLead[1][clID][11]+ '\');" rel="external" data-theme="c">'+responseContactLead[1][clID][10]+'</a></p>';
                contenthtml=contenthtml+'<p style="text-overflow: ellipsis ; white-space: normal;">'+responseContactLead[1][clID][12]+'</p>';
                contenthtml=contenthtml+'</div>';
                    contenthtml=contenthtml+'<div data-role="collapsible">';
                        contenthtml=contenthtml+'<h3>Information</h3>';
                        contenthtml=contenthtml+'<p><strong>'+contenthtmlconcept+'</strong></p>';
                        contenthtml=contenthtml+'<p>Raison Sociale  '+responseContactLead[1][clID][1]+'</p>';
                        contenthtml=contenthtml+'<p><font color="orange">C.A.  '+abbrNum(responseContactLead[1][clID][2],"0")+'€</font></p>';
                        contenthtml=contenthtml+'<p><font color="green">effectif  '+abbrNum(responseContactLead[1][clID][3],"2")+'p</font></p>';
                        contenthtml=contenthtml+'<p style="text-overflow: ellipsis ; white-space: normal;">Lib APE: '+responseContactLead[1][clID][5]+'</p>';
                        contenthtml=contenthtml+'<p>code APE  '+responseContactLead[1][clID][4]+'</p>';
                    contenthtml=contenthtml+'</div>';
                    contenthtml=contenthtml+'<div data-role="collapsible">';
                        contenthtml=contenthtml+'<h3>Contact</h3>';
                        if(typeof responseContactLead[1][clID][6]!=="undefined"){
                        contenthtml=contenthtml+'<p><a href="http://maps.apple.com/?q='+responseContactLead[1][clID][6]+'&nbsp;'+responseContactLead[1][clID][7]+'&nbsp;'+responseContactLead[1][clID][8]+'">'+responseContactLead[1][clID][6]+'</a></p>';
                        contenthtml=contenthtml+'<p>'+responseContactLead[1][clID][7]+'&nbsp;'+responseContactLead[1][clID][8]+'</p>';
                        }else{
                        contenthtml=contenthtml+'<p><a href="http://maps.apple.com/?q='+responseContactLead[1][clID][7]+'&nbsp;'+responseContactLead[1][clID][8]+'">'+responseContactLead[1][clID][7]+'&nbsp;'+responseContactLead[1][clID][8]+'</a></p>';
                        }
                        if(typeof responseContactLead[1][clID][9]!=="undefined"){
                        contenthtml=contenthtml+'<p> <a href=tel:'+responseContactLead[1][clID][9]+'>Tel:'+responseContactLead[1][clID][9]+'</a></p>';
                        }
                    contenthtml=contenthtml+'</div>';
        contenthtml=contenthtml+'</div>';
    contenthtml=contenthtml+'</div>';
}}else{
        contenthtml=contenthtml+'<li data-icon="false" data-mini="true"><a href=""  id="nextRE" data-theme="c" ><center> il n\'y a pas d\'alerte antérieur disponible</center></a></li>';
}
if(responseContactLead[1].length>=10){
contenthtml=contenthtml+'<li  data-icon="false" data-mini="true"><a href="#"  id="nextCL" data-theme="c" onclick="changementpagePlus(\'' +pageCL+ '\',\''+compteN+'\')"><center>page suivante &#9660;</center></a></li>';
}
contenthtml=contenthtml+'</ul>';
document.getElementById(main_contentID).innerHTML=contenthtml;
var theTapList = document.getElementById(main_contentID);
new NoClickDelay(theTapList);
}
