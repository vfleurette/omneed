//___________________________Option__________________________________________________________________________________________
function loadOptionView(main_contentID){
"use strict";
navigator.splashscreen.show();
    var BGTE = "background:#2980B9; text-shadow: none;border: 0px !important;border-width: 0px !important;", BGRE = "background:#C0392B; text-shadow: none;border:0px ;border-width: 0px !important;", BGCO = "background:#8E44AD; text-shadow: none;border:none;border-width: 0px !important;", BGAR = "background:#E67E22; text-shadow: none;border:none;border-width: 0px !important;", BGAL = "background:#27AE60; text-shadow: none;border:none;border-width: 0px !important;", BGCL = "background:#16A085; text-shadow: none;border:none;border-width: 0px !important;", BGOP = "background:#2C3E50; text-shadow: none;border:none;border-width: 0px !important;", BGBL = "background:#101010 ; text-shadow: none;border:none;border-width: 0px !important;", moquette = "background:#101010 ; text-shadow: none;border:none;border-width: 0px !important;";
 var contenthtml='<div data-role="collapsible-set" style="background:#2980B9;"data-theme="c" data-content-theme="c" data-collapsed-icon="arrow-r" data-expanded-icon="arrow-d" data-iconpos="right">';
            contenthtml=contenthtml+collapsibleOption("Territory","Mes Sociétés");
            contenthtml=contenthtml+collapsibleOption("RedList","RedList");
            contenthtml=contenthtml+collapsibleOption("Competitors","Concurents");
            contenthtml=contenthtml+collapsibleOption("Lead","Leads Sociétés");
            contenthtml=contenthtml+collapsibleOption("contactLead","Leads Personne");
contenthtml=contenthtml+'</div>';
  document.getElementById(main_contentID).innerHTML=contenthtml;
}

function collapsibleOption(compte,title){
        $(document).bind("pagecreate", $.fn.checkboxHierarchy);
        var username = window.localStorage.getItem("usernameSTORE"),token = window.localStorage.getItem("tokenSTORE"),typeV = window.localStorage.getItem("typeValue"),responseValues=getAlertConfigValues(username,token,compte),compteAlert,x,y,z;
        var contenthtml='<div data-role="collapsible"  id="collaspible'+compte+'">';

        contenthtml=contenthtml+'<h5><div class="ui-grid-a" data-mini="true">';
                contenthtml=contenthtml+'<div class="ui-block-a"><div data-role="fieldcontain" class="ui-hide-label"><label for="notifsetup'+compte+'">Flip toggle switch:</label><select name="notifsetup'+compte+'" id="notifsetup'+compte+'" data-role="slider" data-mini="true">';
                if(responseValues[2][0][2]=="NO"){
                    contenthtml=contenthtml+'<option value="NO">non</option>';
                    contenthtml=contenthtml+'<option value="YES" selected="">oui</option>';
                }else if(responseValues[2][0][2]=="YES"){
                    contenthtml=contenthtml+'<option value="NO" selected="">non</option>';
                    contenthtml=contenthtml+'<option value="YES" >oui</option>';
                    }
                contenthtml=contenthtml+'</select></div></div>';
        contenthtml=contenthtml+'<div class="ui-block-b">'+title+'</div>';
        contenthtml=contenthtml+'</div></h5><!-- /grid-a -->';

            contenthtml=contenthtml+'<fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">';
                contenthtml=contenthtml+'<legend>Recevoir une synthese par mail</legend>';

                        for (y=1;y<responseValues[2].length;y++)
                        {
                        if(responseValues[2][y][1]==="Pas de notification"){
                            var label="non";
                            }else if(responseValues[2][y][1]==="Pour chaque news"){
                            var label="Non";
                            }else if(responseValues[2][y][1]==="Synthèse journalière"){
                            var label="journalière";
                            }else if(responseValues[2][y][1]==="Synthèse hebdomadaire"){
                            var label="hebdomadaire";
                            }
                        window.localStorage.setItem('Mode'+y, responseValues[2][y][0]);
                        if(responseValues[2][y][2]==="NO")
                        {
                            contenthtml=contenthtml+'<input type="radio" name="Synthese'+compte+'" id="'+responseValues[2][y][0]+'" value="'+responseValues[2][y][0]+'"><label for="'+responseValues[2][y][0]+'">'+label+'</label>';
                        }else if(responseValues[2][y][2]==="YES"){
                            contenthtml=contenthtml+'<input type="radio" name="Synthese'+compte+'" id="'+responseValues[2][y][0]+'" value="'+responseValues[2][y][0]+'" checked="checked"><label for="'+responseValues[2][y][0]+'">'+label+'</label>';
                        }else{
                            contenthtml=contenthtml+'<input type="radio" name="Synthese'+compte+'" id="'+responseValues[2][y][0]+'" value="'+responseValues[2][y][0]+'"><label for="'+responseValues[2][y][0]+'">'+label+'</label>';
                        }
                        }
                    contenthtml=contenthtml+'</fieldset>';
                contenthtml=contenthtml+'<label for="selection" class="selection">Choix thèmes</label>';
                    contenthtml=contenthtml+'<select name="selection'+compte+'" size="18" id="selection'+compte+'" multiple="multiple" data-native-menu="false" class="ui-icon-nodisc"  data-iconshadow="false" data-shadow="false" data-iconpos="left">';
                        contenthtml=contenthtml+'<option>Choisir ses thèmes</option>';
                        for (y=0;y<responseValues[0].length;y++)
                        {
                        if(responseValues[0][y][2]==="NO")
                        {
                            contenthtml=contenthtml+'<option value="'+responseValues[0][y][0]+'" >'+responseValues[0][y][1]+'</option>';
                        }else{
                            contenthtml=contenthtml+'<option value="'+responseValues[0][y][0]+'" selected="">'+responseValues[0][y][1]+'</option>';
                        }
                        }
                    contenthtml=contenthtml+'</select>';

        contenthtml=contenthtml+'<form method="POST" id="tree">';
            contenthtml=contenthtml+'<div data-role="collapsible"  data-inset="false" data-theme="c" data-content-theme="d"  data-collapsed-icon="arrow-d" data-expanded-icon="arrow-u">';
                contenthtml=contenthtml+'<h4>Géographie</h4>';       
                    contenthtml=contenthtml+'<div>';
                        var levelPrec = -1
                        var diff = 0
                        var nbcode=responseValues[1].length;
                        window.localStorage.setItem('nbcode'+compte,responseValues[1].length);
                        for (x=0;x<responseValues[1].length;x++){
                            window.localStorage.setItem('codeGeo'+x, responseValues[1][x][0]);
                            var code = responseValues[1][x][0];
                            var level = parseInt(responseValues[1][x][1]);
                            var desc = responseValues[1][x][2];
                                diff = levelPrec - level;
                                if(  diff < 0) {
                                    contenthtml=contenthtml+'<ul type="none">';
                                    contenthtml=contenthtml+'<hr style="margin-left:-35px;" />';
                                }else if ( diff > 0 ){
                                    contenthtml=contenthtml+'</li></ul>';
                                    contenthtml=contenthtml+'<hr style="margin-left:-35px;" />';
                                }
                                else if ( diff === 0 ){
                                    contenthtml=contenthtml+'</li>';
                                    contenthtml=contenthtml+'<hr style="margin-left:-35px;" />';
                                }
                                if(responseValues[1][x][3]==="NO"){
                                    contenthtml=contenthtml+'<li style="font-size:14px;"><input type="checkbox" style="-webkit-transform: scale(2,2); -webkit-margin-before: 8px;-webkit-margin-start: -41px;" name="'+responseValues[1][x][0]+'" id="'+responseValues[1][x][0]+compte+'"/>'+responseValues[1][x][2];
                                }else{
                                    contenthtml=contenthtml+'<li style="font-size:14px;"><input type="checkbox" style="-webkit-transform: scale(2,2); -webkit-margin-before: 8px;-webkit-margin-start: -41px;" name="'+responseValues[1][x][0]+'" id="'+responseValues[1][x][0]+compte+'" checked/>'+responseValues[1][x][2];
                                }
                            levelPrec=level;
                            }
                        contenthtml=contenthtml+'</div>';
                    contenthtml=contenthtml+'</div>';                          

            contenthtml=contenthtml+'</form>';

    contenthtml=contenthtml+'</div>';
    return contenthtml;
}

//_____________________________________________________________________________________________________________________
var StringUtilities = {
    repeat: function(str, times) {
    "use strict"; 
       return (new Array(times + 1)).join(str);
    }
    //other related string functions...
};
//_____________________________________________________________________________________________________________________
function repeatString(str,times) {
"use strict";
var res='';
for (var i=0;i<times;i++)   {
    res=res+str;
}
return res;
}

//_____________________________________________________________________________________________________________________


function sendSetAlert(anchorCompte,compte){
var SwitchAlert=SwitchSetupNotification(compte);
if( SwitchAlert=="YES"){
var SyntheseMail = SelectMode(compte);
    }else if( SwitchAlert=="NO"){
    var SyntheseMail ="none"
    }
if(window.localStorage.getItem(anchorCompte)>"0"){
var concept = selectConcepts(compte);
var geographie = SelectGeo(compte);
    var value0 = window.localStorage.getItem("usernameSTORE");
    var value2 = window.localStorage.getItem("tokenSTORE");
    var updateStatus=setConfigValues(value0,value2,compte,SyntheseMail,concept,geographie);
    }

}
function SwitchSetupNotification(compte){
    var e = document.getElementById('notifsetup'+compte);
    var setUpnotificationMode = e.options[e.selectedIndex].value;
    return setUpnotificationMode;
}

function memorySetUp(){
sendSetAlert("ActivSetAlertTE","Territory");
sendSetAlert("ActivSetAlertRE","RedList");
sendSetAlert("ActivSetAlertCO","Competitors");
sendSetAlert("ActivSetAlertAL","Lead");
sendSetAlert("ActivSetAlertCL","contactLead");
return "OK";
}

function ActionMemoriser(){
var setupRetour =memorySetUp();
    if(setupRetour==="OK")
    {
        navigator.notification.alert(
            "Vos paramètres sont bien sauvegardés",
            callBackFunctionB, // Specify a function to be called 
            'Bien enregistré !',
            "Retour"
        );
    }else{
        //alert("Erreur de mise à jour !");
        navigator.notification.alert(
            "Erreur de mise à jour !",
            callBackFunctionBerror, // Specify a function to be called 
            'Il y a un problème !',
            "Retour"
        );
}
}

function callBackFunctionB(){
    //console.log('ok');
    navigator.splashscreen.show();
    PanelrefreshPage("Territory","#TerritoryN")
    //window.location.replace("Navigation.html#Territory");
}
function callBackFunctionBerror(){
    //console.log('ok');
    navigator.splashscreen.show();
    window.location.reload(true);
}

function SelectMode(compte){
var radios = document.getElementsByName('Synthese'+compte);

for (var i = 0, length = radios.length; i < length; i++) {
    if (radios[i].checked) {
        var mode = radios[i].value;
    }
}
return mode;
}

function SelectGeo(compte){
var nbcode = window.localStorage.getItem('nbcode'+compte);

    var selectGeo= "";
    for(i=0;i<nbcode;i++)
    {
        var code =window.localStorage.getItem("codeGeo"+i);
        var geocheckbox = document.getElementById(code+compte).checked;
        
    if(geocheckbox==true)
        selectGeo+=code+';';
    }
        ResultatselectGeo=selectGeo.substring(0,selectGeo.length-1);
//alert(ResultatselectGeo);
    return ResultatselectGeo;
}
function selectConcepts(compte){
    var alertTxt = "";
    var selectBoxConcept = document.getElementById('selection'+compte);
    for (var i = 0; i < selectBoxConcept.length; i++) { 

        if (selectBoxConcept.options[i].selected) { 
            alertTxt +=selectBoxConcept.options[i].value+";"; 
        } 
    }
    ResultatSelectConcept=alertTxt.substring(0,alertTxt.length-1);
return ResultatSelectConcept;
}
