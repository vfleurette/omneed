/*jslint browser: true, sloppy: true, white: true, nomen: true, plusplus:true, maxerr: 50, indent: 2 */
/*global jQuery:false, iScroll:false */

/*jshint forin:true, noarg:true, noempty:true, eqeqeq:true, bitwise:true, strict:true,
         undef:true, curly:true, browser:true, jquery:true, indent:2, maxerr:50,
         white:false, nomen:false */


//-------------------------------------------------------
// Pull-down and Pull-up callbacks for "Short Pull" page
//-------------------------------------------------------
/*
(function shortPullPagePullImplementation($) { 
  "use strict";
  var pullDownGeneratedCount = 0,
    pullUpGeneratedCount = 0,
    listSelector = "div.short-pull-demo-page ul.ui-listview",
    lastItemSelector = listSelector + " > li:last-child";
       
  function gotPullDownData(event, data) {


$('#contentDivTE').fadeOut('slow').load('./#contentDivTE').fadeIn("slow");
    $(listSelector).listview("refresh");
    data.iscrollview.refresh ();
    }
/*
  function gotPullUpData(event, data) {
    var i,
        iscrollview = data.iscrollview,
        newContent = "";
    for (i=0; i<3; i+=1) {
      newContent += "<li>Pullup-generated row " + (++pullUpGeneratedCount) + "</li>";
      }
    $(listSelector).append(newContent).listview("refresh");  
    iscrollview.refresh(null, null,
      $.proxy(function afterRefreshCallback() { 
        this.scrollToElement(lastItemSelector, 400);
        }, iscrollview) );
    }
  *//*
  function onPullDown (event, data) { 
    setTimeout(function fakeRetrieveDataTimeout() { 
      gotPullDownData(event, data); }, 
      1500); }  
  
  function onPullUp (event, data) { 
    setTimeout(function fakeRetrieveDataTimeout() { 
      gotPullUpData(event, data);   
      }, 1500); }   
  
  $(document).delegate("div.short-pull-demo-page", "pageinit", 
    function bindShortPullPagePullCallbacks(event) {
      $(".iscroll-wrapper", this).bind( {
      iscroll_onpulldown : onPullDown,
      iscroll_onpullup   : onPullUp
      } );
    }); 
 
  }(jQuery));
*/